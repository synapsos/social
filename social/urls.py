from django.conf.urls import patterns, include, url

# Admin
from django.contrib import admin
admin.autodiscover()

# Views
from forum.views import TopicCreateView

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'social.views.home', name='home'),
	# url(r'^social/', include('social.foo.urls')),

	# Uncomment the admin/doc line below to enable admin documentation:
	# url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

	# Uncomment the next line to enable the admin:
	url(r'^admin/', include(admin.site.urls)),
	url(r'^t/new/$', TopicCreateView.as_view(), name='topic_create'),
)
