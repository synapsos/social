# Forum
from forum.models import *
#from forum.forms import *

# Generic Views
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView

# Render & Redirect
from django.shortcuts import render
from django.http import HttpResponseRedirect as redirect

# Login Required
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

# i18n
from django.utils.translation import ugettext as _


class TopicCreateView(CreateView):
	model = Topic
	template_name_suffix = '_create'
	success_url = '/'
	
	def get_context_data(self, **kwargs):
		context = super(TopicCreateView, self).get_context_data(**kwargs)
		return context
	
#	@method_decorator(login_required)
#	def dispatch(self, *args, **kwargs):
#		return super(TopicCreateView, self).dispatch(*args, **kwargs)
