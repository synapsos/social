from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

class TopicCreateForm(forms.Form):

	title = forms.CharField(
		label = "Title",
		max_length = 100,
		required = True,
	)

	description = forms.TextField(
		label = "Description",
		required = False,
	)
	
	def __init__(self, *args, **kwargs):
		self.helper = FormHelper()
		self.helper.form_id = 'id-topicCreateForm'
		self.helper.form_class = 'blueForms'
		self.helper.form_method = 'post'
		self.helper.form_action = 'topic_create'
		
		self.helper.add_input(Submit('submit', 'Submit'))
		super(TopicCreateForm, self).__init__(*args, **kwargs)
