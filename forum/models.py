from django.db.models import *
from django.contrib.auth.models import *
from django.utils.translation import ugettext as _


#Topic

class TopicManager(Manager):

	def create_topic(self, title, description):
			new_topic = self.create(
				title=title,
				description=decription,
				#admin=
			)
			return new_topic
			
	

class Topic(Model):

	class Meta:
		verbose_name = _('Topic')
		verbose_name_plural = _('Topics')
		
	objects = TopicManager()
	
	def __unicode__(self):
		return self.title
		
	title = CharField(max_length=100, verbose_name=_('Title'))
	description = TextField(verbose_name=_('Description'), blank=True)
	admin = ForeignKey(User, related_name='administrates', verbose_name=_('Admin'), blank=True, null=True)
	
	
class Post(Model):

	class Meta:
		verbose_name = _('Post')
		verbose_name_plural = _('Posts')
		
	#objects = PostManager()
	
	def __unicode__(self):
		return self.title
		
	title = CharField(max_length=100, verbose_name=_('Title'),)
	link = CharField(max_length=100, verbose_name=_('Link'),)
	body = TextField()
	topic = ForeignKey(Topic, related_name='posts', verbose_name=_('Topic'),)
	poster = ForeignKey(User, related_name='posts', verbose_name=_('Poster'),)
	post_date = DateTimeField(auto_now_add=True, verbose_name=_('Posted'),)
	edit_date = DateTimeField(auto_now_add=True, verbose_name=_('Modified'),)
	upvotes = IntegerField(verbose_name=_('Upvotes'), blank=True, null=True,)
	downvotes = IntegerField(verbose_name=_('Downvotes'), blank=True, null=True,)
	points = IntegerField(verbose_name=_('Points'), blank=True, null=True,)
	
	
	def upvote(self):
		self.upvotes += 1
		return True
		
	def downvote(self):
		self.downvotes += 1
		return True
		
	def getPoints(self):
		self.points = self.upvotes - self.downvotes
		return self.points
	
class Comment(Model):

	class Meta:
		verbose_name = _('Comment')
		verbose_name_plural = _('Comments')
		
	#objects = CommentManager()
	
	def __unicode__(self):
		return self.body
		
	user = ForeignKey(User, related_name='comments', verbose_name=_('Commenter'),)
	post = ForeignKey(Post, related_name='comments', verbose_name=_('Post'),)
	parent = ForeignKey('Comment', related_name='replies', verbose_name=_('Parent'),)
	body = TextField()
	post_date = DateTimeField(auto_now_add=True, verbose_name=_('Posted'),)
	upvotes = IntegerField(verbose_name=_('Upvotes'), blank=True, null=True,)
	downvotes = IntegerField(verbose_name=_('Downvotes'), blank=True, null=True,)
	points = IntegerField(verbose_name=_('Points'), blank=True, null=True,)
	
	def upvote(self):
		self.upvotes += 1
		return True
		
	def downvote(self):
		self.downvotes += 1
		return True
		
	def getPoints(self):
		self.points = self.upvotes - self.downvotes
		return self.points
