#Forum
from forum.models import *
from django.contrib import admin

#Auth
from django.contrib.auth.models import User

#i18n
from django.utils.translation import ugettext as _


class PostsInline(admin.StackedInline):
	model = Post
	#readonly_fields = ['title', 'body', 'poster', 'post_date', 'upvotes']
	verbose_name = _('Post')
	verbose_name_plural = _('Posts')
	extra = 0

class TopicAdmin(admin.ModelAdmin):

	def save_model(self, request, obj, form, change):
		if not change:
			obj.admin = User.objects.get(pk=request.user.id)
		obj.save()

	fieldsets = [
		(None,					{'fields': ['title']}),
		(None,					{'fields': ['description']}),
		(None,					{'fields': ['admin']}),
		#(None,					{'fields': ['something']}),
	]
	#exclude = ('admin',)
	#readonly_fields = ['admin']
	list_display = ('title', 'description', 'admin')
	ordering = ['title']
	inlines = [PostsInline]
	search_fields = ['title', 'description']
	#list_filter = ['']
	list_select_related = True
	#actions = [close]
	
class CommentsInline(admin.StackedInline):
	model = Comment
	#readonly_fields = ['title', 'body', 'poster', 'post_date', 'upvotes']
	verbose_name = _('Comment')
	verbose_name_plural = _('Comments')
	extra = 0

class PostAdmin(admin.ModelAdmin):

	def save_model(self, request, obj, form, change):
		if not change:
			obj.poster = User.objects.get(pk=request.user.id)
			obj.post_date = timzone.now()
			obj.upvotes = 1
			obj.downvotes = 0
		obj.edit_date = timzone.now()
		obj.points = obj.upvotes - obj.downvotes
		obj.save()

	fieldsets = [
		(None,					{'fields': ['link']}),
		(None,					{'fields': ['title']}),
		(None,					{'fields': ['body']}),
		(None,					{'fields': ['topic']}),
		#(None,					{'fields': ['something']}),
	]
	#exclude = ('admin',)
	#readonly_fields = ['admin']
	list_display = ('title', 'description', 'admin')
	ordering = ['title']
	inlines = [PostsInline]
	search_fields = ['title', 'description']
	#list_filter = ['']
	list_select_related = True
	#actions = [close]

admin.site.register(Topic, TopicAdmin)
